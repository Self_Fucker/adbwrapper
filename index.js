const ADB = require('adbkit');
const on = require('await-to-js').default;
const { promises:fsp, constants:fsconst } = require('fs');

const defaultBinPath = 'C:\\tools\\adb\\adb.exe';

let client;
let id;
let connected = false;

async function isBinExisted() {
  let existed = true;
  const [errAccess] = await on(fsp.access(defaultBinPath, fsconst.W_OK));
  if (errAccess) {
    existed = false;
    const [err] = await on(fsp.mkdir(defaultBinPath.replace('\\adb.exe', '')));
    if (err) console.error(err);
  }
  return existed;
}

function connect(emulator) {
  return new Promise((resolve, reject) => {
    client = ADB.createClient({ defaultBinPath });
    client.connect('127.0.0.1', emulator === 'mumu' ? '7555' : '62001')
      .then((device) => {
        id = device;
        connected = true;
        return resolve(id);
      })
      .catch((err) => reject(err));
  });
}

function getScreenBuffer() {
  return new Promise((resolve, reject) => {
    const bufferArr = [];
    console.log('find tpl on screen');
    return client.screencap(id)
      .then((screenBuffer) => {
        screenBuffer.on('data', (chunk) => {
          bufferArr.push(chunk);
        });
        screenBuffer.on('end', async () => {
          const buffer = Buffer.concat(bufferArr);
          return resolve(buffer);
        });
      })
      .catch((error) => reject(error));
  });
}

function isConnected() {
  return connected;
}

module.exports = {
  isConnected,
  connect,
  isBinExisted,
  getScreenBuffer,
};
